# TODO #

-un composant fonctionnel Contact.jsx qui reçoit par des props des informations à afficher : un nom, un prénom, un statut "en ligne".

-en fonction des valeurs des props, afficher le JSX du contact avec le nom et le prénom de la personne, et le texte "online" si la personne est en ligne, et "offline" sinon

-appeler plusieurs fois le composant Contact.jsx dans App.jsx (avec des valeurs de props différentes)

-versionner le projet avec Gitlab.