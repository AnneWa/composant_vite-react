import './App.css'
import Contact from './components/Contact';

function App() {
  return (

    <div className="App">

      <h1>Vite + React</h1>

      <h2>Afficher un composant de contact</h2>

      <div className="contact">
        <div>
          <Contact name='Anne' surname='Dupont' statut={true} />
        </div>
        <div>
          <Contact name='Rémi' surname='Dugers' />
        </div>
        <div>
          <Contact name='Soif' surname='Dusudouest' statut={true}/>
        </div>
        <div>
          <Contact name='Léo' surname='Ducoin' />
        </div>
      </div>


      <p className="read-the-docs">
        React est une bibliothèque !
      </p>
    </div>
  )
}





export default App
